#include <vector>
#include <iostream>
#include <fstream>
#include <set>
#include <ctime>
#include <exit.h>
#include <globals.h>
#include <satisfiability_common.h>
#include <Ex2SATSolver.h>

//#include <unistd.h>
//#include <wait.h>

#include "precosat.hh"
#include "precobnr.hh"

using namespace std;
using namespace PrecoSat;

int main (int argc, const char *argv[])
{

//	argv[1] = "inputs/output";
	argv[1] = "18-2-2018_output";
//	argv[1] = "inputs/zenotravel/p03_output";
//	argv[2] = "2";
//	argv[3] = "nowrite";
	argc = 2;

	ifstream input;
	if (argc < 2) {
		cerr << "Usage: " << argv[0] << " PROBLEM" << endl;
		utils::exit_status (utils::ExitCode::INPUT_ERROR);
	}
	//cout << "Requested Problem File = " << argv[1] << endl;
	input.open (argv[1]);
	if (!input.is_open() ) {
		cerr << "Input error." << endl;
		utils::exit_status (utils::ExitCode::INPUT_ERROR);
	}
	read_problem (input);
	input.close();

	int facts = 0;
	for (unsigned int var = 0; var < g_variable_domain.size(); var++) {
		facts += g_variable_domain[var];
	}
	//cout << "Allocated size = " << (g_operators.size() + facts) * 2 * N << endl;
	//cout << "| F | = " <<  facts << endl;
	//cout << "| A | = " <<  g_operators.size() << endl;

	// Create satisfiability variables
	BooleanVariablesDirectory varDir;
	unsigned int aux_vars = 0, aux_vars2 = 0;
	for (unsigned int var = 0; var < g_variable_domain.size(); var++) {
		aux_vars += varDir.get_operators_lookup().lookup_by_var (var).size() - 1;
		aux_vars2 += g_variable_domain[var] - 1;
	}
//    unsigned int start_id;
	unsigned int start_id = 2;
	unsigned long long LITERALS_NUM = (2) + 2 * facts + 2 * aux_vars;
	all_literals.resize (LITERALS_NUM);
	start_id = varDir.generate_literals (0, start_id);

	Extensible2SATSolver *solver2 = new Extensible2SATSolver();
	solver2->initialize (LITERALS_NUM);

	// Create satisfiability clauses
	vector<Clause *> clauses;
//    unsigned int clauses_nbr = 0;
	Clause *cl;

	// 6. Facts Mutex Clause (first layer of fluents)
	//cout << "Started Creating Facts Mutex Clauses..." << endl;
	vector<vector<set<pair<int,int>>>> visited;
	visited.resize (g_variable_domain.size() );
	for (unsigned int var = 0; var < g_variable_domain.size(); var++)
		visited[var].resize (g_variable_domain[var]);
	pair<set<pair<int,int>>::iterator, bool> ret;
	for (unsigned int var = 0; var < g_variable_domain.size(); var++) {
		for (int val = 0; val < g_variable_domain[var]; val++) {
			for (const FactPair &fact : g_inconsistent_facts[var][val]) {
				ret = visited[var][val].insert (make_pair (fact.var, fact.value) );
				ret = visited[fact.var][fact.value].insert (make_pair (var, val) );
				if (ret.second) {
					cl = new Clause ("Facts Mutex");
					cl->add_literal (varDir.lookup_fact (var, val)->get_boolean_literal_at (1)->get_negated() );
					cl->add_literal (varDir.lookup_fact (fact.var,  fact.value)->get_boolean_literal_at (1)->get_negated() );
					solver2->add_clause (cl);
					// cout << clauses_nbr++ << '\r';
				}
			}
		}
	}
	//cout << "Done Creating Facts Mutex Clauses." << endl;

	// Enhanced Facts Mutex Clause (first layer of fluents)
	vector<vector<BooleanVariable *>> auxiliaries2;
	auxiliaries2.resize (g_variable_domain.size() );

	for (unsigned int var = 0; var < g_variable_domain.size(); var++) {

		if (g_variable_domain[var] == 2) {
			cl = new Clause ("Binary SAS+ Mutex");
			cl->add_literal (varDir.lookup_fact (var, 0)->get_boolean_literal_at (1)->get_negated() );
			cl->add_literal (varDir.lookup_fact (var, 1)->get_boolean_literal_at (1)->get_negated() );
			solver2->add_clause (cl);
			//cout << clauses_nbr++ << '\r';
		}

		auxiliaries2[var].resize (g_variable_domain[var] - 1);
		for (int val = 0; val < g_variable_domain[var] - 1; val++) {
			auxiliaries2[var][val] = new BooleanVariable ();
		}

		for (int val = 0; val < g_variable_domain[var] - 1; val++) {
			BooleanLiteral *delta = new BooleanLiteral (auxiliaries2[var][val], 1, Status::TRUE, start_id);
			delta->set_negated (new BooleanLiteral (auxiliaries2[var][val], 1, Status::FALSE, start_id + 1) );
			all_literals[start_id] = delta;
			all_literals[start_id + 1] = delta->get_negated();
			delta->get_negated()->set_negated (delta);
			start_id += 2;
			auxiliaries2[var][val]->add_literal (delta);

			cl = new Clause ("Enhanced Facts Mutex Type 1");
			cl->add_literal (varDir.lookup_fact (var, val)->get_boolean_literal_at (1)->get_negated() );
			cl->add_literal (delta);
			solver2->add_clause (cl);
			//cout << clauses_nbr++ << '\r';


			cl = new Clause ("Enhanced Facts Mutex Type 2");
			cl->add_literal (delta->get_negated() );
			cl->add_literal (varDir.lookup_fact (var, val + 1)->get_boolean_literal_at (1)->get_negated() );
			solver2->add_clause (cl);
			//cout << clauses_nbr++ << '\r';
		}


		for (int i = 0; i < g_variable_domain[var] - 2; i++) {
			cl = new Clause ("Enhanced Facts Mutex Type 3");
			cl->add_literal (auxiliaries2[var][i]->get_literals() [0]->get_negated() );
			cl->add_literal (auxiliaries2[var][i + 1]->get_literals() [0]);
			solver2->add_clause (cl);
			//cout << clauses_nbr++ << '\r';
		}
	}

	// 1. Initial State Clause
	//cout << "Started Creating Initial State Clauses..." << endl;
	for (unsigned int var = 0; var < g_initial_state_data.size(); var++) {
		cl = new Clause ("Initial_State");
		cl->add_literal (varDir.lookup_fact (var, g_initial_state_data[var])->get_boolean_literal_at (1) );
		solver2->add_clause (cl);
		//cout << clauses_nbr++ << '\r';
	}
	//cout << "Done Creating Initial State Clauses." << endl;


	bool success = false;
	for (unsigned int N = 1; !success; N++) {
		cout << "==============================+++++++++++++++++++++++++++++++++++=========================" << endl;
		cout << N << endl;

		solver2->add_fluent_action_layer (2 * (g_operators.size() + facts + aux_vars + aux_vars2) );

		LITERALS_NUM = (2) + ( (g_operators.size() + facts) * 2 * N)
					   + (2 * facts) + (2 * N * aux_vars) + (2 * (N+1) * aux_vars2);
		// 2 * facts because of the N+1 fact layer
		all_literals.resize (LITERALS_NUM);

		start_id = varDir.generate_literals (N, start_id);



		// 3. Actions Additions Clause
		//cout << "Started Creating Action Additions Clauses..." << endl;
		for (GlobalOperator *op : g_operators) {
			for (const GlobalEffect& eff : op->get_effects() ) {
				cl = new Clause ("Actions_Additions");
				cl->add_literal (op->get_op_var()->get_boolean_literal_at (N)->get_negated() );
				cl->add_literal (varDir.lookup_fact (eff.var, eff.val)->get_boolean_literal_at (N + 1) );
				solver2->add_clause (cl);
				//cout << clauses_nbr++ << '\r';
			}
		}

		//cout << "Done Creating Action Additions Clauses." << endl;

		// 4. Actions Precondition Clause
		//cout << "Started Creating Action Preconditions Clauses..." << endl;
		for (GlobalOperator *op : g_operators) {
			for (const GlobalCondition &cond : op->get_preconditions() ) {
				cl = new Clause ("Actions Preconditions");
				cl->add_literal (op->get_op_var()->get_boolean_literal_at (N)->get_negated() );
				cl->add_literal (varDir.lookup_fact (cond.var, cond.val)->get_boolean_literal_at (N) );
				solver2->add_clause (cl);
				//cout << clauses_nbr++ << '\r';
			}
		}
		//cout << "Done Creating Action Preconditions Clauses." << endl;

		//	 5. Enhanced Actions Mutex Clause
		//	cout << "Started Creating Enhanced Actions Mutex Clauses..." << endl;
		vector<vector<BooleanVariable *>> auxiliaries;
		auxiliaries.resize (g_variable_domain.size() );

		for (unsigned int var = 0; var < g_variable_domain.size(); var++) {
			set<GlobalOperator *> guilty_operators_set = varDir.get_operators_lookup().lookup_by_var (var);
			vector<GlobalOperator *> guilty_operators (guilty_operators_set.size() );
			copy (guilty_operators_set.begin(), guilty_operators_set.end(), guilty_operators.begin() );

			auxiliaries[var].resize (guilty_operators.size() - 1);
			for (unsigned int i = 0; i < guilty_operators.size() - 1; i++)
				auxiliaries[var][i] = new BooleanVariable();
			for (unsigned int i = 0; i < guilty_operators.size() - 1; i++) {
				cl = new Clause ("Enhanced Actions Mutex Type 1");
				BooleanLiteral *delta = new BooleanLiteral (auxiliaries[var][i], N, Status::TRUE, start_id);
				delta->set_negated (new BooleanLiteral (auxiliaries[var][i], N, Status::FALSE, start_id + 1) );
				delta->get_negated()->set_negated (delta);
				all_literals[start_id] = delta;
				all_literals[start_id + 1] = delta->get_negated();
				start_id += 2;
				auxiliaries[var][i]->add_literal (delta);

				cl->add_literal (guilty_operators[i]->get_op_var()->get_boolean_literal_at (N)->get_negated() );
				cl->add_literal (delta);

				solver2->add_clause (cl);
//				cout << clauses_nbr++ << '\r';

				cl = new Clause ("Enhanced Actions Mutex Type 2");
				cl->add_literal (delta->get_negated() );
				cl->add_literal (guilty_operators[i + 1]->get_op_var()->get_boolean_literal_at (N)->get_negated() );

				solver2->add_clause (cl);
//				cout << clauses_nbr++ << '\r';
			}
			for (unsigned int i = 0; i < guilty_operators.size() - 2; i++) {
				cl = new Clause ("Enhanced Actions Mutex Type 3");
				cl->add_literal (auxiliaries[var][i]->get_literals() [0]->get_negated() );
				cl->add_literal (auxiliaries[var][i + 1]->get_literals() [0]);

				solver2->add_clause (cl);
//				cout << clauses_nbr++ << '\r';
			}
		}
		//	cout << "Done Creating Enhanced Actions Mutex Clauses." << endl;


		// 6. Facts Mutex Clause
		//cout << "Started Creating Facts Mutex Clauses..." << endl;
		vector<vector<set<pair<int,int>>>> visited;
		visited.resize (g_variable_domain.size() );
		for (unsigned int var = 0; var < g_variable_domain.size(); var++)
			visited[var].resize (g_variable_domain[var]);
		pair<set<pair<int,int>>::iterator, bool> ret;
		for (unsigned int var = 0; var < g_variable_domain.size(); var++) {
			for (int val = 0; val < g_variable_domain[var]; val++) {
				for (const FactPair &fact : g_inconsistent_facts[var][val]) {
					ret = visited[var][val].insert (make_pair (fact.var, fact.value) );
					ret = visited[fact.var][fact.value].insert (make_pair (var, val) );
					if (ret.second) {
						cl = new Clause ("Facts Mutex");
						cl->add_literal (varDir.lookup_fact (var, val)->get_boolean_literal_at (N+1)->get_negated() );
						cl->add_literal (varDir.lookup_fact (fact.var,  fact.value)->get_boolean_literal_at (N+1)->get_negated() );
						solver2->add_clause (cl);
						// cout << clauses_nbr++ << '\r';
					}
				}
			}
		}
		//cout << "Done Creating Facts Mutex Clauses." << endl;

		// 6'. Enhanced Facts Mutex Clause
		//cout << "Started Creating Facts Mutex Clauses..." << endl;
		vector<vector<BooleanVariable *>> auxiliaries2;
		auxiliaries2.resize (g_variable_domain.size() );

		for (unsigned int var = 0; var < g_variable_domain.size(); var++) {

			if (g_variable_domain[var] == 2) {
				cl = new Clause ("Binary SAS+ Mutex");
				cl->add_literal (varDir.lookup_fact (var, 0)->get_boolean_literal_at (N + 1)->get_negated() );
				cl->add_literal (varDir.lookup_fact (var, 1)->get_boolean_literal_at (N + 1)->get_negated() );
				solver2->add_clause (cl);
				//cout << clauses_nbr++ << '\r';
			}

			auxiliaries2[var].resize (g_variable_domain[var] - 1);
			for (int val = 0; val < g_variable_domain[var] - 1; val++) {
				auxiliaries2[var][val] = new BooleanVariable ();
			}

			for (int val = 0; val < g_variable_domain[var] - 1; val++) {
				BooleanLiteral *delta = new BooleanLiteral (auxiliaries2[var][val], N + 1, Status::TRUE, start_id);
				delta->set_negated (new BooleanLiteral (auxiliaries2[var][val], N + 1, Status::FALSE, start_id + 1) );
				delta->get_negated()->set_negated (delta);
				all_literals[start_id] = delta;
				all_literals[start_id + 1] = delta->get_negated();
				start_id += 2;
				auxiliaries2[var][val]->add_literal (delta);

				cl = new Clause ("Enhanced Facts Mutex Type 1");
				cl->add_literal (varDir.lookup_fact (var, val)->get_boolean_literal_at (N + 1)->get_negated() );
				cl->add_literal (delta);
				solver2->add_clause (cl);
				//cout << clauses_nbr++ << '\r';


				cl = new Clause ("Enhanced Facts Mutex Type 2");
				cl->add_literal (delta->get_negated() );
				cl->add_literal (varDir.lookup_fact (var, val + 1)->get_boolean_literal_at (N + 1)->get_negated() );
				solver2->add_clause (cl);
				//cout << clauses_nbr++ << '\r';
			}


			for (int i = 0; i < g_variable_domain[var] - 2; i++) {
				cl = new Clause ("Enhanced Facts Mutex Type 3");
				cl->add_literal (auxiliaries2[var][i]->get_literals() [0]->get_negated() );
				cl->add_literal (auxiliaries2[var][i + 1]->get_literals() [0]);
				solver2->add_clause (cl);
				//cout << clauses_nbr++ << '\r';
			}
		}

		// 7. GODDAMN DISJUNCTION OF HELL
		//cout << "Started Creating GODDAMN DISJUNCTION OF HELL Clauses..." << endl;
		for (unsigned int var = 0; var < g_variable_domain.size(); var++) {
			set<GlobalOperator *> guilty_operators_set = varDir.get_operators_lookup().lookup_by_var (var);
			vector<GlobalOperator *> guilty_operators (guilty_operators_set.size() );
			copy (guilty_operators_set.begin(), guilty_operators_set.end(), guilty_operators.begin() );

			cl = new Clause ("GODDAMN DISJUNCTION OF HELL");
			for (GlobalOperator *op : guilty_operators) {
				cl->add_literal (op->get_op_var()->get_boolean_literal_at (N) );
			}
			solver2->add_clause (cl);
		}
		//cout << "Done Creating GODDAMN DISJUNCTION OF HELL Clauses." << endl;

		for (FactLondex *fl : fact_londexes) {
			int distance = fl->distance;
			for (int forbidden_distance = distance - 1; forbidden_distance > 0; forbidden_distance--) {
				// throw away what is not applicable
				if ( (int) (N + 1) <= forbidden_distance)
					continue;
				// Add what's forbidden for this N
				for (Time t = 1; t <= N - forbidden_distance + 1; t++) {
					if ( (int) (t + forbidden_distance) <= (int) N)
						continue;
					cl = new Clause ("Fact Londex");
					cl->add_literal (varDir.lookup_fact (fl->f1.var,  fl->f1.value)->get_boolean_literal_at (t)->get_negated() );
					cl->add_literal (varDir.lookup_fact (fl->f2.var,  fl->f2.value)->get_boolean_literal_at (t + forbidden_distance)->get_negated() );
					solver2->add_clause (cl);
//					cout << cl->dump() << endl;
					//cout << clauses_nbr++ << '\r';
				}
			}
		}

		for (OpLondex *ol : operators_londexes) {
			int distance = ol->distance;
			for (int forbidden_distance = distance - 1; forbidden_distance > 0; forbidden_distance--) {
				// throw away what is not applicable
				if ( (int) (N) <= forbidden_distance)
					continue;
				// Add what's forbidden for this N
				for (Time t = 1; t <= N - forbidden_distance; t++) {
					if ( (int) (t + forbidden_distance) <= (int) N)
						continue;
					cl = new Clause ("Operator Londex");
					cl->add_literal (ol->op1->get_op_var()->get_boolean_literal_at (t)->get_negated() );
					cl->add_literal (ol->op1->get_op_var()->get_boolean_literal_at (t + forbidden_distance)->get_negated() );
					solver2->add_clause (cl);
//					cout << cl->dump() << endl;
					//cout << clauses_nbr++ << '\r';
				}
			}
		}

		cout << "==============================+++++++++++++++++++++++++++++++++++=========================" << endl;

		// 2. set Goal State to true
		for (pair<int,int> fact_pair : g_goal) {
			solver2->propagate (varDir.lookup_fact (fact_pair.first, fact_pair.second)->get_boolean_literal_at (N + 1)->get_id(), Status::TRUE);
			cl = new Clause("Goal State Clause");
			cl->add_literal(varDir.lookup_fact (fact_pair.first, fact_pair.second)->get_boolean_literal_at (N + 1));
			solver2->add_clause(cl);
//			solver->assign (varDir.lookup_fact (fact_pair.first, fact_pair.second)->get_boolean_literal_at (N + 1)->get_id(), Status::TRUE);
//			solver->assign (varDir.lookup_fact (fact_pair.first, fact_pair.second)->get_boolean_literal_at (N + 1)->get_negated()->get_id(), Status::FALSE);
		}

//        map<unsigned int, Status> assignment = solver->get_assignment();
//			BooleanLiteral *l;
//			for (unsigned int i = 2; i < LITERALS_NUM; i++) {
//				l = all_literals[i];
//				cout << l->dump() << " = " << assignment[l->get_id()] << endl;
//			}
//
//        for (Clause *cl : solver->get_clauses()) {
//			cout << cl->dump() << endl;
//        }

//		int fd[2];
//		pipe (fd);
//
//		pid_t pid = fork();
//		if (pid == -1) {
//			return EXIT_FAILURE;
//			// error, no child created
//		} else if (pid == 0) {
//			// child
//			close (fd[1]);
//			close (0);
//			dup (fd[0]);
//			char * const p [] = {{}};
//			execv ("./precosat-576-7e5e66f-120112/precosat", p);
//		} else {
//			// parent
//			close(fd[0]);
//
//			ostringstream os;
//			os << "p cnf " << (all_literals.size() - 2) / 2 << " " << solver->get_clauses().size() << endl;
//			BooleanLiteral *l;
//			for (Clause *cl : solver->get_clauses())
//			{
//				int clause_size = cl->literals.size();
//				for (int i = 0; i < clause_size; i++)
//				{
//					l = cl->literals[i];
//					if (l->get_status() == Status::FALSE)
//						os << "-";
//					os << l->get_id() / 2 << " ";
//				}
//				os << '0' << endl;
//			}
//			cout << os.str();
//			const char *sso = os.str().c_str();
//			size_t len = os.str().length();
//
//			write(fd[1], sso, len);
//            close(fd[1]);          /* Reader will see EOF */
//
//			int status;
//			if (waitpid (pid, &status, 0) == -1) {
//				// handle error
//			} else {
//				cout << "WAITING" << endl;
//				if (WIFEXITED (status) && !WEXITSTATUS (status) ) {
//					/* the program terminated normally and executed successfully */
//				} else if (WIFEXITED (status) && WEXITSTATUS (status) ) {
//					if (WEXITSTATUS (status) == 127) {
//						/* execl() failed */
//					} else {
//						/* the program terminated normally, but returned a non-zero status */
//						switch (WEXITSTATUS (status) ) {
//						case 10:
//							utils::exit_status(utils::PLAN_FOUND);
//						default:
//							continue;
//						}
//					}
//				} else {
//					/* the program didn't terminate normally */
//				}
//
//			}
//		}

		for (Clause *cl : solver2->get_clauses())
			cout << cl->dump() << endl;
		Solver solver;
		solver.init((LITERALS_NUM - 2) / 2);
		solver.set("quiet", 1);
		solver.set("verbose", 0);
		solver.set ("decompose",0);
		solver.set ("autark",0);
		solver.set ("probe",0);
		solver.set ("otfs",0);
		solver.set ("elimasym", 0);
		solver.set ("elim",0);
		solver.set ("blockrtc",2);
		solver.set ("block",0);
		solver.set ("elimrtc",2);
		solver.fxopts ();
        for (Clause *cl : solver2->get_clauses()) {
			for (BooleanLiteral *l : cl->literals) {
				solver.add(l->get_id());
			}
			solver.add(0);
        }
        int res;
        if ((res = solver.solve()) > 0) {
			cout << res << endl;
			if (solver.satisfied())
				utils::exit_status(utils::PLAN_FOUND);
			else
				utils::exit_status(utils::CRITICAL_ERROR);
        }


//		success = solver->solve();
//		if (success) {
//			map<unsigned int, Status> assignment = solver->get_assignment();
//			BooleanLiteral *l;
//			for (unsigned int i = 2; i < LITERALS_NUM; i++) {
//				l = all_literals[i];
//				cout << l->dump() << " = " << assignment[l->get_id()] << endl;
//			}
//		}


		// 2. set Goal State to undefined
//		for (pair<int,int> fact_pair : g_goal) {
//			solver->propagate (varDir.lookup_fact (fact_pair.first, fact_pair.second)->get_boolean_literal_at (N + 1)->get_id(), Status::UNDEFINED);
//			solver->assign (varDir.lookup_fact (fact_pair.first, fact_pair.second)->get_boolean_literal_at (N + 1)->get_id(), Status::UNDEFINED);
//			solver->assign (varDir.lookup_fact (fact_pair.first, fact_pair.second)->get_boolean_literal_at (N + 1)->get_negated()->get_id(), Status::UNDEFINED);
//		}


	}















//
//
////	for (Clause *cl : clauses) {
////		cout << cl->dump() << endl;
////	}
//
//    //cout << "-----------------------------" << endl;
//
////	bool **conn;
////	int size;
////	group_cliques (clauses, conn, size);
//
////	ostringstream input_string;
////	input_string << (all_literals.size() - 2) / 2 << " " << clauses.size() << endl; // M N
//
//    ofstream translation_output;
//
//    if (write)
//        translation_output.open("translation.out");
//
////	ofstream implication_graph;
////	implication_graph.open("implication_graph.gv");
////	implication_graph << "digraph I {" << endl;
//
//    time_t now = time(0);
//    if (write)
//    {
//        translation_output << "c Plan Length: " << N << endl;
//        translation_output << "c Created: " << ctime(&now);
//        translation_output << "c Literal IDs Mapping" << endl;
//        for (unsigned int i = 2; i < all_literals.size() - 1; i+=2)
//        {
//            translation_output << "c " << (i/2) << "->" << all_literals[i]->dump() << "," << endl;
//        }
//
//        translation_output << "p cnf " << (all_literals.size() - 2) / 2 << " " << clauses.size() << endl;
//    }
//
//    cout << "c Plan Length: " << N << endl;
//    cout << "c Created: " << ctime(&now);
//    cout << "c Literal IDs Mapping" << endl;
//    for (unsigned int i = 2; i < all_literals.size() - 1; i+=2)
//    {
//        cout << "c " << (i/2) << "->" << all_literals[i]->dump() << "," << endl;
//    }
//
//    cout << "p cnf " << (all_literals.size() - 2) / 2 << " " << clauses.size() << endl;
//
//
//    for (Clause *cl : clauses)
//    {
//        int clause_size = cl->literals.size();
//#ifdef NDEBUG
//        assert(clause_size >= 2);
//#endif
//
//        BooleanLiteral *l;
//
////		if (clause_size == 2) {
////			l = cl->literals[0]->get_negated();
////			implication_graph << "\t";
////			if (l->get_status() == Status::FALSE)
////				implication_graph << "-";
////			implication_graph << l->get_id() / 2 << " -> ";
////			l = cl->literals[1];
////			if (l->get_status() == Status::FALSE)
////				implication_graph << "-";
////			implication_graph << l->get_id() / 2 << " [arrowsize=.5, weight=2.];" << endl;
////			l = cl->literals[1]->get_negated();
////			implication_graph << "\t";
////			if (l->get_status() == Status::FALSE)
////				implication_graph << "-";
////			implication_graph << l->get_id() / 2 << " -> ";
////			l = cl->literals[0];
////			if (l->get_status() == Status::FALSE)
////				implication_graph << "-";
////			implication_graph << l->get_id() / 2 << " [arrowsize=.5, weight=2.];" << endl;
////		}
//        if (write)
//        {
//            for (int i = 0; i < clause_size; i++)
//            {
//                l = cl->literals[i];
//                if (l->get_status() == Status::FALSE)
//                    translation_output << "-";
//                translation_output << l->get_id() / 2 << " ";
//            }
//            translation_output << '0' << endl;
//        }
//
//        for (int i = 0; i < clause_size; i++)
//        {
//            l = cl->literals[i];
//            if (l->get_status() == Status::FALSE)
//                cout << "-";
//            cout << l->get_id() / 2 << " ";
//        }
//        cout << '0' << endl;
//
////        if (clause_size == 1) {
////			l = cl->literals[0];
////			if (l->get_status() == Status::FALSE)
////				input_string << "-";
////			input_string << l->get_id() / 2 << " " << l->get_id() / 2 << endl;
////		} else {
////			l = cl->literals[0];
////			if (l->get_status() == Status::FALSE)
////				input_string << "-";
////			input_string << l->get_id() / 2 << " ";
////			l = cl->literals[1];
////			if (l->get_status() == Status::FALSE)
////				input_string << "-";
////			input_string << cl->literals[1]->get_id() << endl;
////		}
//    }
//
////    implication_graph << "}";
//
//    if (write)
//        translation_output.close();
////    implication_graph.close();
//
//    //cout << "Created translation file 'translation.out'" << endl;
//    //cout << "Cleaning up..." << endl;
//
//
////    initialize(LITERALS_NUM);
////    solve(input_string.str());
//

	for (Clause *cl : solver2->get_clauses() )
		delete cl;

//	for (unsigned int var = 0; var < g_variable_domain.size(); var++) {
//		for (unsigned int aux = 0; aux < auxiliaries[var].size(); aux++) {
//			delete auxiliaries[var][aux];
//		}
//	}

	return 0;
}
