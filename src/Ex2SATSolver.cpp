#include <Ex2SATSolver.h>
#include <stack>
#include <assert.h>

size_t Extensible2SATSolver::LITERAL_ID = 1;

Extensible2SATSolver::Extensible2SATSolver()
{

}

void Extensible2SATSolver::initialize(unsigned long long literals)
{
	for (unsigned int i = 0; i < literals; i++) {
		this->assignment[i] = Status::UNDEFINED;
	}
	this->literals_size = literals;
	this->implication_graph.resize(literals);
}

void Extensible2SATSolver::add_fluent_action_layer(unsigned long long additional_literals)
{
	for (unsigned int i = 0; i < additional_literals; i++) {
		this->assignment[i + this->literals_size] = Status::UNDEFINED;
	}
	this->literals_size += additional_literals;
	this->implication_graph.resize(this->literals_size);
}

bool Extensible2SATSolver::propagate(unsigned int id, Status status)
{
    return this->propagate(id, this->assignment, status);
}


bool Extensible2SATSolver::propagate (unsigned int id, map<unsigned int, Status> &tmp_assignment, Status status)
{
	assert(status != Status::FALSE);
	stack<unsigned int> q;
	q.push (id);
	while (!q.empty() ) {
		id = q.top();
		q.pop();
		for (unsigned int affected : implication_graph[id])
			q.push (affected);
		if (status == TRUE && tmp_assignment[id] == Status::FALSE)
			return false; // conflict
		if (id % 2 == 0) { // id is the positive form of the var
            if (status == Status::UNDEFINED) {
                tmp_assignment[id] = Status::UNDEFINED;
                tmp_assignment[id + 1] = Status::UNDEFINED;
            } else {
                tmp_assignment[id] = Status::TRUE;
                tmp_assignment[id + 1] = Status::FALSE;
            }
		} else { // id is the negative form of the var
            if (status == Status::UNDEFINED) {
                tmp_assignment[id - 1] = Status::UNDEFINED;
                tmp_assignment[id] = Status::UNDEFINED;
            } else {
                tmp_assignment[id - 1] = Status::FALSE;
                tmp_assignment[id] = Status::TRUE;
            }
		}
	}
	return true;
}

bool Extensible2SATSolver::add_clause (Clause* cl)
{
	this->clauses.push_back (cl);
	size_t n = cl->literals.size();
	if (n > 2) {
		vector<unsigned int> literals;
		for (BooleanLiteral *literal : cl->literals) {
			literals.push_back (literal->get_id());
		}
		// insert sort based on size of the clause
		size_t i = 0;
		vector<vector<unsigned int>>::iterator it = branches.begin();
		while (i < branches.size() && branches[i++].size() <= n);
		branches.insert (it + i, literals);
	} else if (n == 2) {
		implication_graph[cl->literals[0]->get_negated()->get_id()].push_back (cl->literals[1]->get_id() );
		implication_graph[cl->literals[1]->get_negated()->get_id()].push_back (cl->literals[0]->get_id() );
	} else {
		// cause true assignment and propagate through implication graph
		BooleanLiteral *l = cl->literals[0];
		if (!this->propagate (l->get_id(), this->assignment, Status::TRUE) )
			return false;
	}
	return true;
}

bool Extensible2SATSolver::solve()
{
	vector<vector<unsigned int>> remaining_branches;
	//remaining_branches.resize (this->variables_size); // assign does it anyway, maybe it's faster that way?
	// copy(branches.begin(), branches.end(), remaining_branches);
	remaining_branches.assign (branches.begin(), branches.end() );
	return solve (assignment, remaining_branches);
}

bool Extensible2SATSolver::solve (map<unsigned int, Status> current_assignment, vector<vector<unsigned int>> remaining_branches)
{
	if (remaining_branches.empty() ) {
		// satisfiable
		// perpetuate the current_assignment
		this->assignment = current_assignment;
		return true;
	}

	// select a branch
	vector<unsigned int> selected = remaining_branches[0];
	remaining_branches.erase (remaining_branches.begin() );

	size_t branching = selected.size();

	for (unsigned int i = 0; i < branching; i++) {

		if (current_assignment[selected[i]] == Status::FALSE)
			continue; // conflict

		// Need to propagate the assignment through the implication graph
		// Create a deep copy dummy assignment for the occasion
		map<unsigned int, Status> branched_assignment = current_assignment;
		// Propagate on branched_assignment...
		if (!this->propagate (selected[i], branched_assignment, Status::TRUE) )
			continue;

//		return solve (branched_assignment, remaining_branches);
		bool success = solve (branched_assignment, remaining_branches);
		if (!success) {
			continue;

			//current_assignment[selected[i]] = Status::FALSE;
		} else {
			// retain this branching
			return true;
		}
	}

	return false;
}

void Extensible2SATSolver::assign (unsigned int id, Status status)
{
	this->assignment[id] = status;
}

map<unsigned int, Status> const & Extensible2SATSolver::get_assignment() const
{
	return this->assignment;
}

vector<Clause *> const& Extensible2SATSolver::get_clauses() const
{
	return this->clauses;
}


