#include <limits>
#include <fstream>
#include <regex>
#include <globals.h>
#include <exit.h>

using namespace std;

bool g_use_metric;
int g_min_action_cost = numeric_limits<int>::max();
int g_max_action_cost = 0;
vector<string> g_variable_name;
vector<int> g_variable_indices;
vector<FlattenedOperator *> g_flat_operators;
vector<int> g_variable_domain;
vector<int> g_facts_bits;
vector<vector<FactPair>> g_facts;
vector<int> g_start_indices;
vector<vector<string>> g_fact_names;
vector<int> g_axiom_layers;
vector<int> g_default_axiom_values;
vector<int> g_initial_state_data;
vector<pair<int, int>> g_goal;
vector<FactID> g_flat_goal;
vector<GlobalOperator *> g_operators;
vector<GlobalOperator *> g_axioms;
vector<vector<set<FactPair>>> g_inconsistent_facts;
vector<FactLondex *> fact_londexes;
vector<OpLondex *> operators_londexes;


void check_magic (istream &in, string magic)
{
	string word;
	in >> word;
	if (word != magic) {
		cout << "Failed to match magic word '" << magic << "'." << endl;
		cout << "Got '" << word << "'." << endl;
		if (magic == "begin_version") {
			cerr << "Possible cause: you are running the planner "
				 << "on a preprocessor file from " << endl
				 << "an older version." << endl;
		}
		utils::exit_status (utils::ExitCode::INPUT_ERROR);
	}
}

void read_and_verify_version (istream &in)
{
	int version;
	check_magic (in, "begin_version");
	in >> version;
	check_magic (in, "end_version");
	if (version != PRE_FILE_VERSION) {
		cerr << "Expected preprocessor file version " << PRE_FILE_VERSION
			 << ", got " << version << "." << endl;
		cerr << "Exiting." << endl;
		utils::exit_status (utils::ExitCode::INPUT_ERROR);
	}
}

void read_metric (istream &in)
{
	check_magic (in, "begin_metric");
	in >> g_use_metric;
	check_magic (in, "end_metric");
}

void read_variables (istream &in)
{
	int count;
	int index;
	in >> count;
	int layer;
	int range;
	string name;
	int startIndex = 0;
	for (int i = 0; i < count; ++i) {
		check_magic (in, "begin_variable");
		in >> name;
		index = stoi (name.substr (3) );
		g_variable_indices.push_back (index);
		g_variable_name.push_back (name);
		in >> layer;
		g_axiom_layers.push_back (layer);
		in >> range;
		g_variable_domain.push_back (range);
		g_start_indices.push_back (startIndex);
		startIndex += range;
		in >> ws;
		vector<string> fact_names (range);
		for (size_t j = 0; j < fact_names.size(); ++j)
			getline (in, fact_names[j]);
		g_fact_names.push_back (fact_names);
		check_magic (in, "end_variable");
	}
	g_variable_name.resize (count);
	g_variable_indices.resize (count);
	g_variable_domain.resize (count);
	g_start_indices.resize (count);
	g_axiom_layers.resize (count);
	g_fact_names.resize (count);
	g_facts.resize (count);

	for (unsigned int var = 0; var < g_variable_domain.size(); var++) {
		g_facts[var].resize (g_variable_domain[var]);
		for (int val = 0; val < g_variable_domain[var]; val++) {
			g_facts[var][val] = FactPair (var, val);
		}
	}
}

void read_mutexes (istream &in)
{
	g_inconsistent_facts.resize (g_variable_domain.size() );
	for (size_t i = 0; i < g_variable_domain.size(); ++i)
		g_inconsistent_facts[i].resize (g_variable_domain[i]);

	int num_mutex_groups;
	in >> num_mutex_groups;

	/* NOTE: Mutex groups can overlap, in which case the same mutex
	   should not be represented multiple times. The current
	   representation takes care of that automatically by using sets.
	   If we ever change this representation, this is something to be
	   aware of. */

	for (int i = 0; i < num_mutex_groups; ++i) {
		check_magic (in, "begin_mutex_group");
		int num_facts;
		in >> num_facts;
		vector<FactPair> invariant_group;
		invariant_group.reserve (num_facts);
		for (int j = 0; j < num_facts; ++j) {
			int var;
			int value;
			in >> var >> value;
			invariant_group.emplace_back (var, value);
		}
		check_magic (in, "end_mutex_group");
		for (const FactPair &fact1 : invariant_group) {
			for (const FactPair &fact2 : invariant_group) {
				if (fact1.var != fact2.var) {
					/* The "different variable" test makes sure we
					   don't mark a fact as mutex with itself
					   (important for correctness) and don't include
					   redundant mutexes (important to conserve
					   memory). Note that the preprocessor removes
					   mutex groups that contain *only* redundant
					   mutexes, but it can of course generate mutex
					   groups which lead to *some* redundant mutexes,
					   where some but not all facts talk about the
					   same variable. */
					g_inconsistent_facts[fact1.var][fact1.value].insert (fact2);
				}
			}
		}
	}
}

void read_goal (istream &in)
{
	check_magic (in, "begin_goal");
	int count, fact;
	int var, val;
	in >> count;
	if (count < 1) {
		cerr << "Task has no goal condition!" << endl;
		utils::exit_status (utils::ExitCode::INPUT_ERROR);
	}
	for (int i = 0; i < count; ++i) {
		in >> var >> val;
		g_goal.push_back (make_pair (var, val) );
		fact = 0;
		for (int j = 0; j < var; j++)
			fact += g_variable_domain[j];
		fact += val;
		g_flat_goal.push_back (fact);
	}
	check_magic (in, "end_goal");
	g_goal.resize (count);
	g_flat_goal.resize (count);

}

void read_operators (istream &in)
{
	int count;
	in >> count;

	int var, val;
	FactID fact;
	vector<GlobalCondition> preconditions;
	vector<GlobalEffect> effects;
	FlattenedOperator *flat_op;
	GlobalOperator *o;
	for (int i = 0; i < count; ++i) {
		flat_op = new FlattenedOperator();
		o = new GlobalOperator (in, false, i);
		g_operators.push_back (o);
		preconditions = o->get_preconditions();
		effects = o->get_effects();
		for (unsigned int k = 0; k < preconditions.size(); k++) {
			fact = 0;
			var = preconditions[k].var;
			val = preconditions[k].val;
			for (int j = 0; j < var; j++)
				fact += g_variable_domain[j];
			fact += val;
			flat_op->preconditions.push_back (fact);
		}
		for (unsigned int k = 0; k < effects.size(); k++) {
			fact = 0;
			var = effects[k].var;
			val = effects[k].val;
			for (int j = 0; j < var; j++)
				fact += g_variable_domain[j];
			fact += val;
			flat_op->effects.push_back (fact);
		}
		g_flat_operators.push_back (flat_op);

	}
}

void read_axioms (istream &in)
{
	int count;
	in >> count;
	for (int i = 0; i < count; ++i)
		g_axioms.push_back (new GlobalOperator (in, true, i) );

}

void read_problem (istream &in)
{
//	cout << "verifying version..." << endl;
	read_and_verify_version (in);
//	cout << "reading metric..." << endl;
	read_metric (in);
//	cout << "reading variables..." << endl;
	read_variables (in);
//	cout << "reading mutexes..." << endl;
	read_mutexes (in);
//	cout << "reading initial state..." << endl;
	g_initial_state_data.resize (g_variable_domain.size() );
	check_magic (in, "begin_state");
	for (size_t i = 0; i < g_variable_domain.size(); ++i) {
		in >> g_initial_state_data[i];
	}
	check_magic (in, "end_state");
	g_default_axiom_values = g_initial_state_data;

//	cout << "reading goal state..." << endl;
	read_goal (in);
//	cout << "reading operators..." << endl;
	read_operators (in);

	for (unsigned int var = 0; var < g_variable_domain.size(); var++) {
		for (int val = 0; val < g_variable_domain[var]; val++) {
			vector<GlobalCondition> conditions;
			vector<GlobalEffect> effects;
			conditions.push_back (GlobalCondition (var, val) );
			effects.push_back (GlobalEffect (var, val, vector<GlobalCondition>() ) );
			ostringstream os;
			os << "dum_" << var << "_" << val;
			g_operators.push_back (new GlobalOperator (os.str(), conditions, effects, false, 0) );
		}
	}

//	cout << "reading axioms..." << endl;
	read_axioms (in);

//	cout << "done reading input!" << endl;
//	cout << "done initializing global data" << endl;
//	cout << g_variable_domain.size () << " variables of domains: [";
//	for (unsigned int var = 0; var < g_variable_domain.size() - 1; var++)
//		cout << g_variable_domain[var] << ", ";
//	cout << g_variable_domain[g_variable_domain.size() - 1];
//	cout << "]" << endl;

	// Reading londex file if available
	ifstream londex;
	londex.open ("londex.ldm");
	if (!londex.is_open() ) {
		// Londex file unavailable
		return;
	}

	int facts, ops;
	string tmp;
	smatch match;
	regex e ("(.+\\(.+\\)) (.+\\(.+\\)) ([0-9]+)");

	bool found;
	int p;
	londex >> facts;
	FactLondex *fl;
	getline (londex, tmp);

	for (int i = 0; i < facts; i++) {
		getline (londex, tmp);
		std::regex_search (tmp, match, e);
		p = 0;
		fl = new FactLondex();
		for (auto x : match) {
			if (p == 0) {
				p++;
				continue;
			} else if (p == 1) {
				found = false;
				for (unsigned int var = 0; var < g_variable_domain.size(); var++) {
					for (int val = 0; val < g_variable_domain[var]; val++) {
						if (g_fact_names[var][val].rfind (x.str() ) != std::string::npos) {
							found = true;
							fl->f1 = FactPair (var, val);
							goto done;
						}
					}
				}
			} else if (p == 2) {
				found = false;
				for (unsigned int var = 0; var < g_variable_domain.size(); var++) {
					for (int val = 0; val < g_variable_domain[var]; val++) {
						if (g_fact_names[var][val].rfind (x.str() ) != std::string::npos) {
							found = true;
							fl->f2 = FactPair (var, val);
							goto done;
						}
					}
				}
			} else if (p == 3) {
				fl->distance = stoi (x.str() );
				if (fl->distance < 2) {
					found = false;
				} else {
					fact_londexes.push_back (fl);
				}
			}
done:
			if (!found) {
				delete fl;
				goto again;
			}
			p++;
		}
again:
		;
	}

	regex e2 ("(.+)#(.+)#([0-9]+)");
	OpLondex *ol;
	londex >> ops;

	getline (londex, tmp);
	for (int i = 0; i < ops; i++) {
		getline (londex, tmp);
		std::regex_search (tmp, match, e2);
		p = 0;
		ol = new OpLondex();
		for (auto x : match) {
			if (p == 0) {
				p++;
				continue;
			} else if (p == 1) {
				found = false;
				for (GlobalOperator *op : g_operators) {
					if (op->get_name().rfind (x.str() ) != std::string::npos) {
						found = true;
						ol->op1 = op;
						break;
					}
				}
			} else if (p == 2) {
				found = false;
				for (GlobalOperator *op : g_operators) {
					if (op->get_name().rfind (x.str() ) != std::string::npos) {
						found = true;
						ol->op2 = op;
						break;
					}
				}
			} else if (p == 3) {
				ol->distance = stoi (x.str() );
				if (ol->distance < 2) {
					found = false;
				} else {
					operators_londexes.push_back (ol);
				}
			}
			if (!found) {
				delete ol;
				goto again2;
			}
			p++;
		}
again2:
		;
	}
//
//    for (FactLondex *fl : fact_londexes) {
//		cout << g_fact_names[fl->f1.var][fl->f1.value] << "->" << g_fact_names[fl->f2.var][fl->f2.value] << "==>" << fl->distance << endl;
//    }
//
//    for (OpLondex *ol : operators_londexes) {
//		cout << ol->op1->get_name() << "->" << ol->op2->get_name() << "==>" << ol->distance << endl;
//    }

}

template<class T>
bool in_bounds (int index, const T &container)
{
	return index >=0 && (unsigned int) index < container.size();
}

static void check_fact (int var, int val)
{
	if (!in_bounds (var, g_variable_domain) ) {
		cerr << "Invalid variable id: " << var << endl;
		utils::exit_status (utils::ExitCode::INPUT_ERROR);
	}
	if (val < 0 || val >= g_variable_domain[var]) {
		cerr << "Invalid value for variable " << var << ": " << val << endl;
		utils::exit_status (utils::ExitCode::INPUT_ERROR);
	}
}

GlobalCondition::GlobalCondition (istream &in)
{
	in >> var >> val;
	check_fact (var, val);
}

GlobalCondition::GlobalCondition (int variable, int value)
	: var (variable),
	  val (value)
{
	check_fact (var, val);
}

GlobalEffect::GlobalEffect (int variable, int value, const vector<GlobalCondition> &conds)
	: var (variable),
	  val (value),
	  conditions (conds)
{
	check_fact (var, val);
}

void GlobalOperator::read_pre_post (istream &in)
{
	int cond_count, var, pre, post;
	in >> cond_count;
	vector<GlobalCondition> conditions;
	conditions.reserve (cond_count);
	for (int i = 0; i < cond_count; ++i)
		conditions.push_back (GlobalCondition (in) );
	in >> var >> pre >> post;
	if (pre != -1)
		check_fact (var, pre);
	check_fact (var, post);
	if (pre != -1)
		preconditions.push_back (GlobalCondition (var, pre) );
	effects.push_back (GlobalEffect (var, post, conditions) );
}

GlobalOperator::GlobalOperator (istream &in, bool axiom, int op_id)
{
	this->op_id = op_id;
	this->preferred = false;
	is_an_axiom = axiom;
	if (!is_an_axiom) {
		check_magic (in, "begin_operator");
		in >> ws;
		getline (in, name);
		int count;
		in >> count;
		for (int i = 0; i < count; ++i)
			preconditions.push_back (GlobalCondition (in) );
		in >> count;
		for (int i = 0; i < count; ++i)
			read_pre_post (in);

		int op_cost;
		in >> op_cost;
		cost = g_use_metric ? op_cost : 1;

		g_min_action_cost = min (g_min_action_cost, cost);
		g_max_action_cost = max (g_max_action_cost, cost);

		check_magic (in, "end_operator");
	} else {
		name = "<axiom>";
		cost = 0;
		check_magic (in, "begin_rule");
		read_pre_post (in);
		check_magic (in, "end_rule");
	}
	this->op_var = new OperatorBooleanVariable (this);
}

GlobalOperator::GlobalOperator (std::string name, std::vector<GlobalCondition> preconditions, std::vector<GlobalEffect> effects, bool is_an_axiom, int cost)
{
	this->name = name;
	this->preconditions = preconditions;
	this->effects = effects;
	this->is_an_axiom = is_an_axiom;
	this->cost = cost;
	this->op_var = new OperatorBooleanVariable (this);
}

GlobalOperator::~GlobalOperator()
{
	delete this->op_var;
}

void GlobalCondition::dump() const
{
	cout << g_variable_name[var] << ": " << val;
}

void GlobalEffect::dump() const
{
	cout << g_variable_name[var] << ":= " << val;
	if (!conditions.empty() ) {
		cout << " if";
		for (size_t i = 0; i < conditions.size(); ++i) {
			cout << " ";
			conditions[i].dump();
		}
	}
}

void GlobalOperator::dump() const
{
	cout << name << ":";
	for (size_t i = 0; i < preconditions.size(); ++i) {
		cout << " [";
		preconditions[i].dump();
		cout << "]";
	}
	for (size_t i = 0; i < effects.size(); ++i) {
		cout << " [";
		effects[i].dump();
		cout << "]";
	}
	cout << endl;
}

FactProxy::FactProxy (int var_id, int value)
	:fact (var_id, value) {}

FactProxy::FactProxy (const FactPair &fact)
	:fact (fact) {}

int FactProxy::get_variable() const
{
	return fact.var;
}

int FactProxy::get_value() const
{
	return fact.value;
}

FactPair FactProxy::get_pair() const
{
	return fact;
}

const std::string &FactProxy::get_name() const
{
	return g_variable_name[fact.var];
}

bool FactProxy::operator== (const FactProxy &other) const
{
	return fact == other.fact;
}

bool FactProxy::operator!= (const FactProxy &other) const
{
	return ! (*this == other);
}

bool FactProxy::is_mutex (const FactProxy &other) const
{
	set<FactPair> group = g_inconsistent_facts[fact.var][fact.value];
	return group.find (other.fact) != group.end();
}

FactPair::FactPair (int var, int value)
	: var (var), value (value) {}

FactPair::FactPair()
{
}

bool FactPair::operator< (const FactPair &other) const
{
	return var < other.var || (var == other.var && value < other.value);
}

bool FactPair::operator== (const FactPair &other) const
{
	return var == other.var && value == other.value;
}

bool FactPair::operator!= (const FactPair &other) const
{
	return var != other.var || value != other.value;
}

