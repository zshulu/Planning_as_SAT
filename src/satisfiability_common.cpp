#include <satisfiability_common.h>
#include <ext/mcqd/mcqd.h>
#include <string>
#include <cstring>
#include <sstream>
#include <map>
#include <typeinfo>
#include <fstream>

/**
	Utilities
*/
vector<BooleanLiteral *> indexed_literals;
vector<BooleanLiteral *> all_literals;
BooleanLiteral *BooleanLiteral::FALSE_LITERAL = new BooleanLiteral (nullptr, 0, Status::FALSE, 0);
BooleanLiteral *BooleanLiteral::TRUE_LITERAL = new BooleanLiteral (nullptr, 0, Status::TRUE, 1);

void group_cliques (const vector<Clause *> &clauses, bool** &conn, int &size)
{
	// insert clauses into an undirected graph
	// also check the clauses are pairwise distinct
	set<int> v;
	set<BooleanLiteral *> visited;
	multimap<int, int> e;
	unsigned int id;
	unsigned int ids [2];
	BooleanLiteral *l;
	pair<set<BooleanLiteral *>::iterator, bool> ret;

	indexed_literals[0] = BooleanLiteral::FALSE_LITERAL;
	indexed_literals[1] = BooleanLiteral::TRUE_LITERAL;

	for (const Clause* clause : clauses) {
		assert (clause->literals.size() <= 2);
		// Update Vertex Set

		for (unsigned int literal = 0; literal < 2; literal++) {
			l = clause->literals[literal];

			ret = visited.insert (l->get_negated() );
			if (ret.second) {
				// insertion took place, new element
				id = l->get_negated()->get_id();
				v.insert (id);
				indexed_literals[id] = l->get_negated();
			} else {
				// already seen this literal
				id = (*ret.first)->get_id();
			}
			ids[literal] = id;
		}

		// Update Edge Set
		e.insert (make_pair (ids[0], ids[1]) );
	}
	ofstream output;
	output.open ("output.gv");
	output << "graph G {" << endl;
	for (BooleanLiteral *bl : indexed_literals) {
		if (!bl)
			continue;
		if (dynamic_cast<OperatorBooleanVariable *> (bl->get_boolean_var() ) )
			output << "\t \"" << bl->dump() << "\" [color=red];" << endl;
	}
	for (auto it = e.begin(); it != e.end(); it++)
		output << "\t\"" << indexed_literals[it->first]->dump() << "\" -- \"" << indexed_literals[it->second]->dump() << "\";" << endl;
	output << "}" << endl;
	output.close();

	output.open ("cliques.txt");

	clock_t start2;
	do {
		start2 = clock();
		// Construct adjacency graph
		size = *v.rbegin() + 1;
		cout << "AGAIN = " << v.size() <<  endl;
		for (auto it = v.begin(); it != v.end(); it++)
			cout << indexed_literals[*it]->dump() << " ";
		cout << endl;
		cout << endl;
		conn = new bool *[size];
		for (int i=0; i < size; i++) {
			conn[i] = new bool[size];
			memset (conn[i], 0, size * sizeof (bool) );
		}
		for (multimap<int,int>::iterator it = e.begin(); it != e.end(); it++) {
			conn[it->first][it->second] = true;
			conn[it->second][it->first] = true;
		}
		cout << "|E| = " << e.size() << "  |V| = " << v.size() << " p = " << (double) e.size() / (v.size() * (v.size() - 1) / 2) << endl;

		// Detect maximum clique
		int *qmax;
		int qsize;
		Maxclique m (conn, size);
		m.mcqdyn (qmax, qsize); // run max clique with improved coloring
		output << "Maximum clique: " << endl;
		for (int i = 0; i < qsize; i++)
			output << '\t' << qmax[i] << '\t' << indexed_literals[qmax[i]]->dump() << endl;
		output << "Size = " << qsize << endl;
		output << "Number of steps = " << m.steps() << endl;
		output << "Time = " << ( (double) (clock() - start2) ) / CLOCKS_PER_SEC << endl << endl;

		// Remove the clique from conn ...
		// ... and repeat
		int rem_id;
		for (int i = 0; i < qsize; i++) {
			rem_id = qmax[i];
			v.erase (rem_id);
			e.erase (rem_id);
			vector<multimap<int,int>::iterator> iterators;
			for (multimap<int,int>::iterator it = e.begin(); it != e.end(); it++) {
				if (it->second == rem_id)
					iterators.push_back (it);
			}
			for (const multimap<int,int>::iterator &it : iterators)
				e.erase (it);

		}
//		cout << "graph G {" << endl;
//		for (BooleanLiteral *bl : indexed_literals) {
//			if (!bl) continue;
//			if (dynamic_cast<OperatorBooleanVariable *>(bl->get_boolean_var()))
//				cout << "\t\"" << bl->dump() << "\" [color=red];" << endl;
//		}
//		for (auto it = e.begin(); it != e.end(); it++)
//			cout << "\t\"" << indexed_literals[it->first]->dump() << "\" -- \"" << indexed_literals[it->second]->dump() << "\";" << endl;
//		cout << "}" << endl;

		delete [] qmax;
		for (int i = 0; i < size; i++)
			delete [] conn[i];
		delete [] conn;
	} while (!v.empty() && !e.empty() );

	output.close();
	// return ???
}

/**
	Boolean Variable
*/
void BooleanVariable::set_boolean_literal_at (BooleanLiteral* l, Time t)
{
	this->literals[t - 1] = l;
}

string BooleanVariable::dump() const
{
	return "BooleanVar";
}

/**
	Fact Boolean Variable
*/
FactBooleanVariable::~FactBooleanVariable()
{
	delete this->fact;
}

string FactBooleanVariable::dump() const
{
	return "[FACT][" + g_fact_names[this->fact->var][this->fact->value] + "]";
}

void FactBooleanVariable::allocate_for_plan_length (unsigned int N, unsigned int &start_id)
{
	this->literals.resize (N + 1);
	BooleanLiteral *l;
//	for (Time t = 1; t <= N + 1; t++) {
	l = new BooleanLiteral (this, N + 1, Status::TRUE, start_id);
	all_literals[start_id] = l;
	l->set_negated (new BooleanLiteral (this, N + 1, Status::FALSE, start_id + 1) );
	l->get_negated()->set_negated (l);
	all_literals[start_id + 1] = l->get_negated();
	this->set_boolean_literal_at (l, N + 1);
	start_id += 2;
//	}
}

/**
	Operator Boolean Variable
*/
void OperatorBooleanVariable::allocate_for_plan_length (unsigned int N, unsigned int &start_id)
{
	this->literals.resize (N);
	BooleanLiteral *l;
//	for (Time t = 1; t <= N; t++) {
	l = new BooleanLiteral (this, N, Status::TRUE, start_id);
	all_literals[start_id] = l;
	l->set_negated (new BooleanLiteral (this, N, Status::FALSE, start_id + 1) );
	l->get_negated()->set_negated (l);
	all_literals[start_id + 1] = l->get_negated();
	this->set_boolean_literal_at (l, N);
	start_id += 2;
//	}
}

string OperatorBooleanVariable::dump() const
{
	return "[OPERATOR][" + this->op->get_name() + "]";
}

/**
	Boolean Literal
*/
BooleanVariable* BooleanLiteral::get_boolean_var()
{
	return this->var;
}

BooleanLiteral* BooleanVariable::get_boolean_literal_at (Time t)
{
	return this->literals[t - 1];
}

Time BooleanLiteral::get_time()
{
	return this->time;
}

Status BooleanLiteral::get_status()
{
	return this->status;
}

string BooleanLiteral::dump()
{
	ostringstream os;
	os << '#' << id << "--";
	if (this == FALSE_LITERAL)
		return os.str() + "FALSE";
	else if (this == TRUE_LITERAL)
		return os.str() + "TRUE";
	string status_sign = (!status) ?"!":"";
	os << status_sign << var->dump() << "@" << time;
	return os.str();
}

void BooleanLiteral::set_id (unsigned int id)
{
	this->id = id;
}

unsigned int BooleanLiteral::get_id() const
{
	return this->id;
}

BooleanLiteral* BooleanLiteral::get_negated()
{
	if (this == FALSE_LITERAL)
		BooleanLiteral::FALSE_LITERAL->negated = BooleanLiteral::TRUE_LITERAL;
	else if (this == TRUE_LITERAL)
		BooleanLiteral::TRUE_LITERAL->negated = BooleanLiteral::FALSE_LITERAL;
	return this->negated;
}

void BooleanLiteral::set_negated (BooleanLiteral *neg)
{
	this->negated = neg;
}


/**
	Boolean Variables Directory
*/
BooleanVariablesDirectory::BooleanVariablesDirectory()
{
	this->fact_variables.resize (g_variable_domain.size() );
	for (unsigned int var = 0; var < g_variable_domain.size(); var++) {
		this->fact_variables[var].resize (g_variable_domain[var]);
		for (int val = 0; val < g_variable_domain[var]; val++) {
			this->fact_variables[var][val] = new FactBooleanVariable (new FactPair (var, val) );
		}
	}
}

BooleanVariablesDirectory::~BooleanVariablesDirectory()
{
	for (unsigned int var = 0; var < g_variable_domain.size(); var++) {
		for (int val = 0; val < g_variable_domain[var]; val++) {
			delete this->fact_variables[var][val];
		}
	}
}

FactBooleanVariable* BooleanVariablesDirectory::lookup_fact (unsigned int var, int val)
{
	return this->fact_variables[var][val];
}

OperatorsLookup& BooleanVariablesDirectory::get_operators_lookup()
{
	return this->operators;
}

unsigned int BooleanVariablesDirectory::generate_literals (const unsigned int N, unsigned int &start_id)
{
	// Facts Variables
	for (unsigned int var = 0; var < g_variable_domain.size(); var++) {
		for (FactBooleanVariable *f : this->fact_variables[var]) {
			f->allocate_for_plan_length (N, start_id);
		}
	}

	if (N == 0)
		return start_id;
	// Operators Variables
	for (GlobalOperator *op : g_operators) {
		op->get_op_var()->allocate_for_plan_length (N, start_id);
	}

	return start_id;
}

/**
	Operators Lookup
*/
OperatorsLookup::OperatorsLookup()
{
	this->lookup_by_effect_vector.resize (g_variable_domain.size() );
	this->lookup_by_precondition_vector.resize (g_variable_domain.size() );
	this->group_by_affected_var.resize (g_variable_domain.size() );
	for (unsigned int var = 0; var < g_variable_domain.size(); var ++) {
		this->lookup_by_effect_vector[var].resize (g_variable_domain[var]);
		this->lookup_by_precondition_vector[var].resize (g_variable_domain[var]);
	}

	for (GlobalOperator *op : g_operators) {
		for (const GlobalEffect &eff : op->get_effects() ) {
			this->lookup_by_effect_vector[eff.var][eff.val].push_back (op);
			this->group_by_affected_var[eff.var].insert (op);
		}
		for (const GlobalCondition &cond : op->get_preconditions() ) {
			this->lookup_by_precondition_vector[cond.var][cond.val].push_back (op);
		}
	}
}

OperatorsLookup::~OperatorsLookup()
{
	for (GlobalOperator *op : g_operators) {
		delete op;
	}
	for (FlattenedOperator *op : g_flat_operators) {
		delete op;
	}
}

vector<GlobalOperator *> OperatorsLookup::lookup_by_precondition (unsigned int var, unsigned int val)
{
	return lookup_by_precondition_vector[var][val];
}

vector<GlobalOperator *> OperatorsLookup::lookup_by_effect (unsigned int var, unsigned int val)
{
	return lookup_by_effect_vector[var][val];
}

set<GlobalOperator *> OperatorsLookup::lookup_by_var (unsigned int var)
{
	return this->group_by_affected_var[var];
}

