CC=g++
#CFLAGS=-g -Wall
CFLAGS=-Ofast -std=c++11 -DNLOGPRECO 
SRC_DIR=src
OBJ_DIR=obj
BIN_DIR=bin
INC=-Iinclude -Iprecosat-576-7e5e66f-120112
EXE=planner

all: preco deps
	$(CC) $(CFLAGS) -c $(SRC_DIR)/*.cpp $(INC)
	mv *.o $(OBJ_DIR)
	$(CC) $(OBJ_DIR)/*.o -o $(BIN_DIR)/$(EXE)

preco:
	$(CC) $(CFLAGS) -c precosat-576-7e5e66f-120112/precobnr.cc
	$(CC) $(CFLAGS) -c precosat-576-7e5e66f-120112/precosat.cc
	mv precobnr.o precosat.o $(OBJ_DIR)

deps:
	mkdir -p $(OBJ_DIR)
	mkdir -p $(BIN_DIR)

clean:
	rm -rf $(OBJ_DIR)/*
	rm -rf $(BIN_DIR)/*

Debug: all

Release: all
