#pragma once
#include <vector>
#include <map>
#include <satisfiability_common.h>

using namespace std;

typedef vector<int> neighbors;

class Extensible2SATSolver {

public:
	static size_t LITERAL_ID;
	Extensible2SATSolver();
	void initialize(unsigned long long);
	bool add_clause (Clause *cl);
	bool solve();
	void assign (unsigned int, Status);
	void add_fluent_action_layer(unsigned long long);
	bool propagate (unsigned int, map<unsigned int, Status> &, Status);
	bool propagate (unsigned int, Status);
	map<unsigned int, Status> const &get_assignment() const;
	vector<Clause *> const &get_clauses() const;

private:
	vector<neighbors> implication_graph;
	vector<vector<unsigned int>> branches;
	vector<Clause *> clauses;
	size_t literals_size;
	size_t clauses_size;
	map<unsigned int, Status> assignment;

	bool solve (map<unsigned int, Status>, vector<vector<unsigned int>>);
};

