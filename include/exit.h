#ifndef EXIT_H
#define EXIT_H
#include <stdlib.h>
namespace utils {
enum ExitCode {
	PLAN_FOUND = 0,
	CRITICAL_ERROR = 1,
	INPUT_ERROR = 2,
	UNSUPPORTED = 3,
	// Task is provably unsolvable with given bound.
	UNSOLVABLE = 4,
	// Search ended without finding a solution.
	UNSOLVED_INCOMPLETE = 5,
	OUT_OF_MEMORY = 6
};

void exit_status (int status);

}
#endif /* EXIT_H */

