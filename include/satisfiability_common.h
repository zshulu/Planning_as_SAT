#pragma once
#include <globals.h>
#include <string>
#include <set>

using namespace std;

typedef unsigned int Time;

enum Status {
	UNDEFINED = -1,
	FALSE = 0,
	TRUE = 1
};

class BooleanVariable;

class BooleanLiteral {
public:
	static BooleanLiteral *FALSE_LITERAL;
	static BooleanLiteral *TRUE_LITERAL;
	BooleanLiteral (BooleanVariable *var, Time time, Status status, unsigned int id)
		:var (var), time (time), status (status), negated (0), id (id) {}
	BooleanVariable * get_boolean_var ();
	Time get_time ();
	Status get_status ();
	string dump ();
	void set_id (unsigned int id);
	unsigned get_id() const;
	BooleanLiteral *get_negated();
	void set_negated (BooleanLiteral *neg);

private:
	BooleanVariable *var;
	Time time;
	Status status;
	BooleanLiteral *negated;
	unsigned int id;
};

class BooleanVariable {
public:
	virtual string dump() const;
	BooleanVariable() {};
	virtual ~BooleanVariable() {
		for (BooleanLiteral *l : this->literals) {
			delete l->get_negated();
			delete l;
		}
	};
	virtual void allocate_for_plan_length (unsigned int N, unsigned int &start_id) {};
	BooleanLiteral* get_boolean_literal_at (Time t);
	void set_boolean_literal_at (BooleanLiteral *l, Time t);
	void add_literal (BooleanLiteral *l) {
		this->literals.push_back (l);
	}
	vector<BooleanLiteral *> get_literals () {
		return this->literals;
	}
protected:
	vector<BooleanLiteral *> literals;
};

struct FactPair;

class FactBooleanVariable
	: public BooleanVariable {
public:
	FactBooleanVariable (FactPair *fact)
		:fact (fact) {}
	~FactBooleanVariable();
	void allocate_for_plan_length (unsigned int N, unsigned int &start_id);
	string dump() const;

private:
	FactPair *fact;
};

class GlobalOperator;

class OperatorBooleanVariable
	: public BooleanVariable {
public:
	OperatorBooleanVariable (GlobalOperator *op)
		:op (op) {}
	string dump() const;
	void allocate_for_plan_length (unsigned int N, unsigned int &start_id);

private:
	GlobalOperator *op;
};

extern vector<BooleanLiteral *> indexed_literals;
extern vector<BooleanLiteral *> all_literals;

class OperatorsLookup {
public:
	OperatorsLookup();
	~OperatorsLookup();
	vector<GlobalOperator *> lookup_by_precondition (unsigned int var, unsigned int val);
	vector<GlobalOperator *> lookup_by_effect (unsigned int var, unsigned int val);
	set<GlobalOperator *> lookup_by_var (unsigned int var);

private:
	vector<vector<vector<GlobalOperator *>>> lookup_by_precondition_vector;
	vector<vector<vector<GlobalOperator *>>> lookup_by_effect_vector;
	vector<set<GlobalOperator *>> group_by_affected_var;
};

class BooleanVariablesDirectory {
public:
	BooleanVariablesDirectory ();
	~BooleanVariablesDirectory ();
	FactBooleanVariable * lookup_fact (unsigned int var, int val);
	OperatorsLookup & get_operators_lookup ();
	unsigned int generate_literals (const unsigned int N, unsigned int &start_id);

private:
	vector<vector<FactBooleanVariable *>> fact_variables;
	OperatorsLookup operators;
};

struct Clause {
	Clause (string clause_name) {
		this->clause_name = clause_name;
	}

	~Clause () {
//		for (BooleanLiteral *l : this->literals) {
//			if (l->get_negated()) {
//				cout << "Destroyed (neg) " << l->get_negated()->get_id() << endl;
//				delete l->get_negated();
//			}
//			if (l) {
//				cout << "Destroyed " << l->get_id() << endl;
//				delete l;
//			}
//		}
	}

	void add_literal (BooleanLiteral *literal) {
		literals.push_back (literal);
	}

	string dump () {
		string s = "[" + this->clause_name + "]" + "{ ";
		for (BooleanLiteral *l : this->literals) {
			s += l->dump() + " OR ";
		}
		return s.substr (0, s.length() - 3) + "}";
	}

	vector<BooleanLiteral *> literals;
	string clause_name;
};

void group_cliques (const vector<Clause *> &clauses, bool **&conn, int &size);
