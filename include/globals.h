#pragma once
#include <iostream>
#include <vector>
#include <set>
#include <sstream>
#include <satisfiability_common.h>
using namespace std;

static const int PRE_FILE_VERSION = 3;

typedef unsigned short FactID;

struct FactPair {
	int var;
	int value;
	FactPair ();
	FactPair (int var, int value);
	bool operator< (const FactPair &other) const;
	bool operator== (const FactPair &other) const;
	bool operator!= (const FactPair &other) const;
};

struct GlobalCondition {
	int var;
	int val;
	GlobalCondition (std::istream &in);
	GlobalCondition (int variable, int value);
	void dump() const;
};

struct GlobalEffect {
	int var;
	int val;
	std::vector<GlobalCondition> conditions;
	GlobalEffect (std::istream &in);
	GlobalEffect (int variable, int value, const std::vector<GlobalCondition> &conds);
	void dump() const;
};

class OperatorBooleanVariable;

class GlobalOperator {
	int op_id;
	bool preferred;
	bool is_an_axiom;
	std::vector<GlobalCondition> preconditions;
	std::vector<GlobalEffect> effects;
	std::string name;
	int cost;
	OperatorBooleanVariable *op_var;

	void read_pre_post (std::istream &in);
public:
	explicit GlobalOperator (std::istream &in, bool is_axiom, int op_id);
	explicit GlobalOperator (std::string name, std::vector<GlobalCondition> preconditions, std::vector<GlobalEffect> effects, bool is_an_axiom, int cost);
	~GlobalOperator();
	void dump() const;
	const std::string &get_name() const {
		return name;
	}

	bool is_axiom() const {
		return is_an_axiom;
	}
	bool is_preferred() const {
		return this->preferred;
	}
	void set_preferred() {
		this->preferred = true;
	}
	void clear_preferred() {
		this->preferred = false;
	}

	const std::vector<GlobalCondition> &get_preconditions() const {
		return preconditions;
	}
	const std::vector<GlobalEffect> &get_effects() const {
		return effects;
	}

	int get_cost() const {
		return cost;
	}
	int get_id() const {
		return op_id;
	}

	OperatorBooleanVariable * get_op_var() const {
		return this->op_var;
	}
};

class FactProxy {
	FactPair fact;
public:
	FactProxy (int var_id, int value);
	FactProxy (const FactPair &fact);
	~FactProxy() = default;

	int get_variable() const;
	int get_value() const;
	FactPair get_pair() const;
	const std::string &get_name() const;
	bool operator== (const FactProxy &other) const;
	bool operator!= (const FactProxy &other) const;
	bool is_mutex (const FactProxy &other) const;
};

struct FlattenedOperator {
	std::vector<FactID> preconditions;
	std::vector<FactID> effects;
};

struct OpLondex {
	GlobalOperator *op1, *op2;
	unsigned int distance;
};

struct FactLondex {
	FactPair f1, f2;
	unsigned int distance;
};

void read_problem (istream &);
extern bool g_use_metric;
extern vector<string> g_variable_name;
extern vector<int> g_variable_indices;
extern vector<FlattenedOperator *> g_flat_operators;
extern vector<int> g_variable_domain;
extern vector<vector<FactPair>> g_facts;
extern vector<int> g_facts_bits;
extern vector<int> g_start_indices;
extern vector<vector<string>> g_fact_names;
extern vector<int> g_axiom_layers;
extern vector<int> g_default_axiom_values;
extern vector<vector<set<FactPair>>> g_inconsistent_facts;

extern int g_min_action_cost;
extern int g_max_action_cost;
extern vector<int> g_initial_state_data;
extern vector<pair<int, int>> g_goal;
extern vector<FactID> g_flat_goal;
extern vector<GlobalOperator *> g_operators;
extern vector<GlobalOperator *> g_axioms;
extern vector<FactLondex *> fact_londexes;
extern vector<OpLondex *> operators_londexes;
