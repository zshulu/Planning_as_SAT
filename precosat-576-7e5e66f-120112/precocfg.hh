/**********************************************************/
/* Automatically generated by './mkconfig': do note edit! */
/**********************************************************/
#define PRECOSAT_CXX "g++ (Debian 6.3.0-18) 6.3.0 20170516"
#define PRECOSAT_CXXFLAGS "-O3 -Wall -Wextra -DNDEBUG -DNLOGPRECO -DNSTATSPRECO $(PROF)"
#define PRECOSAT_OS "Linux prometheus 4.9.0-4-686-pae i686"
#define PRECOSAT_COMPILED "Sat Oct 21 18:50:17 EEST 2017"
#define PRECOSAT_RELEASED "Thu Jan 12 07:59:44 CET 2012"
#define PRECOSAT_VERSION "576"
#define PRECOSAT_ID "7e5e66faa15ff83953915cba4b46afe851cd5847"
