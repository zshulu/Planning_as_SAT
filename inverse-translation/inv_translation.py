#!/usr/bin/env python
import re
import json, operator
from itertools import groupby
from sys import argv

#translation_file = 'translation.out'
#sat_output_file = '27032018_0858.sat'
translation_file = argv[1]
sat_output_file = argv[2]

translation = []
grouped = {'operators': [],'facts': []}

with open(translation_file) as t:
    i = 0
    for line in t:
        if i != 3:
            i += 1
            continue
        if not line.startswith('c'):
            break
        try:
            (id, old_id, type, name, time) = re.findall('c ([0-9]+)->#([0-9]+)--\[([A-Z]+)\]\[(.+)\]@([0-9]+),', line)[0]
        except IndexError:
            # Ignore the auxiliary variables
            continue
        translation.append(
            {
                'id': id,
                'old_id': old_id,
                'type': type,
                'name': name,
                'time': time,
                'value': 0
            }
        )

with open(sat_output_file) as s:
    for line in s:
        if not line.startswith('v'):
            continue
        values = line[2:].split()
        for value in values:
            if value == '0':
                continue
            if value.startswith('-'):
                try:
                    translation[int(value[1:]) - 1]['value'] = False
                except IndexError:
                    continue
            else:
                try:
                    translation[int(value) - 1]['value'] = True
                except IndexError:
                    continue


#translation.sort(key=operator.itemgetter('id'))
translation.sort(key=lambda x: int(x['time']))
#translation.sort(key=operator.itemgetter('type'))

for literal in translation:
    if False or (not literal['name'][0:3] == 'dum' and literal['value']):
        if literal['type'] == 'OPERATOR':
            grouped['operators'].append(literal)
        elif literal['type'] == 'FACT':
            grouped['facts'].append(literal)
        else:
            continue

grouped_str = json.dumps(grouped, indent=4)
f = open('values.json', 'w')
print >> f, grouped_str
f.close()

time = 1
print "[ TIME 1 --"
for operator in grouped['operators']:
    if not operator['time'] == str(time):
        print "-- ]"
        print "[ TIME", time + 1, "--"
        time += 1
    print "\t" + operator['name']

