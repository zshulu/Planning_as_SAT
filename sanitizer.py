#!/usr/bin/env python

def sanitize_fact(fact):
    tmp = fact.split('-')
    predicate = tmp[0]
    parameters = tmp[1:]
    parameters = ', '.join(parameters)
    return predicate + '(' + parameters + ')'

def sanitize_op(op):
    tmp = op.split(' ')
    predicate = tmp[0]
    parameters = tmp[1:]
    parameters = ' '.join(parameters)
    return predicate + ' ' + parameters

facts = open("facts").readlines()
ops = open("ops").readlines()

print len(facts)
for fact in facts:
    fact = fact.split(')')
    time = int(fact[1][1:])
    fact = map(str.lower, fact[0][1:].split(' -> '))
    fact = map(sanitize_fact, fact)
    print ' '.join(fact) + ' ' + str(time)

print len(ops)
for op in ops:
    op = op.split(')')
    time = int(op[1][1:])
    op = map(str.lower, op[0][1:].split(' -> '))
    op = map(sanitize_op, op)
    print '#'.join(op) + '#' + str(time)

